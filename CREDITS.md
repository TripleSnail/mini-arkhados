# Thanks to #

Anonymous for sword_throw. Original can be found from
[soundbible](http://soundbible.com/1607-Throw-Knife.html). sword_throw is in public domain.

Bart Kelsey for fireball_cast. fireball\_cast is multilicensed under multiple licenses and one of them is CC-BY-SA 3.0, which we use for this project. Original work can be found from [OpenGameArt](http://opengameart.org/content/spell-4-fire).

Mike Koenig for sword swing. Sword swing is licensed under CC-BY 3.0. Original work can be fround from [SoundBible](http://soundbible.com/1176-Sword-Swing.html).

Menu music by [Matthew Pablo](www.matthewpablo.com). Menu music is called "Dream Raid" and it's licensed under CC-BY 3.0. Original music can be found from [OpenGameArt](http://opengameart.org/content/dream-raid-cinematic-action-soundtrack) or from Matthew Pablo's [website](www.matthewpablo.com).

Edward J. Blakeley for music 'heroism'. heroism is licensed under multiple licenses and one of them is GPLv3, which we use for this project. Origian work can be found from [OpenGameArt](http://opengameart.org/content/heroism)

Daniel Cook for fireball. Fireball is based on Daniel Cook's "Sun" which is used in game called "Lost Garden". Sun is licensed under CC-BY 3.0 and can be found from [OpenGameArt](http://opengameart.org/content/iron-plague-sun1bmp).

Cuzco for explosion graphics. explosion is licensed under CC0(Public Domain). Original work can be found from [OpenGameArt](http://opengameart.org/content/explosion).
