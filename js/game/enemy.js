/*This file is part of mini-Arkhados.

mini-Arkhados is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

mini-Arkhados is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with mini-Arkhados.  If not, see <http://www.gnu.org/licenses/>.*/

var game = game || {};

game.EnemyEntity = me.ObjectEntity.extend({
    walkDirection: new me.Vector2d(),
    speedMovement: 3,
    maxHealth: 3100,
    health: 3100,
    stunned: 0,
    attackRange: 35,
    meleeCD: 0,
    chargeCD: 0,
    chargeLeft: 0,
    damageFactor: 1,
    daggerCD: 0,
    siCD: 0,
    SIDuration: 0,
    currentAnim: null,
    animationDirection: "down",
    actionAnimationPending: false,
    ownAlive: true,

    init: function(x, y, settings) {
        this.parent(x, y, settings);
        this.anchorPoint.x = 0.5;
        this.anchorPoint.y = 1;
        this.type = me.game.ENEMY_OBJECT;
        this.text = new me.Font("Verdana", 14, "green");
        this.alwaysUpdate = true;
        this.collidable = true;
        this.NORMAL_SPEED = 4;
        this.CHARGING_SPEED = 10;
        this.renderable.addAnimation("walk-up", [104,105,106,107,108,109,110,111,112]);
        this.renderable.addAnimation("walk-left", [117,118,119,120,121,122,123,124,125]);
        this.renderable.addAnimation("walk-down", [130,131,132,133,134,135,136,137,138]);
        this.renderable.addAnimation("walk-right",[143,144,145,146,147,148,149,150,151]);

        this.renderable.addAnimation("spear-up", [52,53,54,55,56,57,58,59]);
        this.renderable.addAnimation("spear-left", [65,66,67,68,69,70,71,72]);
        this.renderable.addAnimation("spear-down", [78,79,80,81,82,83,84,85]);
        this.renderable.addAnimation("spear-right", [91,92,93,94,95,96,97,98]);

        this.renderable.addAnimation("charge-up", [57]);
        this.renderable.addAnimation("charge-left", [70]);
        this.renderable.addAnimation("charge-down", [83]);
        this.renderable.addAnimation("charge-right", [96]);

        this.renderable.addAnimation("death", [260,261,262,263,264,265]);
    },

    hit: function() {
        this.player.health -= this.damageFactor * 170;
        this.meleeCD = 1;
        me.audio.play("sword_sound", false, null, 0.3);

        var newAnimName = "spear-" + this.animationDirection;
        this.actionAnimationPending = true;
        var self = this;
        this.changeAnim(newAnimName, function() {
            self.actionAnimationPending = false;
        });
    },

    charge: function() {
        this.chargeCD = 8;
        this.chargeLeft = 200;
        this.speedMovement = 10;
        this.changeAnim("charge-" + this.animationDirection);
        this.actionAnimationPending = true;
    },

    castSI: function() {
        me.audio.play("fight_growl", false, null, 0.4);
        var healthPercent = this.health / this.maxHealth;
        var inverseHealthPercent = 1.0 - healthPercent;
        this.damageFactor = 1 + healthPercent / 10;
        var originalSpeed = this.speedMovement;
        this.speedMovement *= 1 + inverseHealthPercent / 10;
        this.speedMovement += 2;
        this.siCD = 12;
        this.SIDuration = 3;
    },

    onCollision: function(res, obj) {

    },

    castDagger: function(dir) {
        var dagger = me.entityPool.newInstanceOf('dagger', this.pos.x, this.pos.y);
        me.game.add(dagger, 3);
        dagger.setDirection(dir);
        this.daggerCD = 5;
    },

    update: function() {
        if (!this.ownAlive) {
            this.parent();
            return true;
        }
        this.renderable.animationpause = false;
        this.player = me.game.getEntityByName("mainPlayer")[0];
        if (this.health <= 0) {
            var self = this;
            this.ownAlive = false;
            this.changeAnim("death", function() {
                game.endMatch();
                me.game.remove(self);
                me.player_won = true;
            });
            return true;
        }

        this.meleeCD -= 0.017;
        this.chargeCD -= 0.017;
        this.daggerCD -= 0.017;
        this.siCD -= 0.017;
        this.stunned -= 0.017;
        this.SIDuration -= 0.017;
        this.decrementCharging();

         if (this.SIDuration <= 0 && this.chargeLeft <= 0) {
            this.speedMovement = 4;
            this.damageFactor = 1;
         }

        if (this.stunned > 0) {
            this.renderable.animationpause = true;
            this.actionAnimationPending = false;
            this.vel.x = 0;
            this.vel.y = 0;

        } else {

            var distanceToPlayer = this.pos.distance(this.player.pos);
            if (this.chargeLeft <= 0) {
                if (this.siCD <= 0 && (distanceToPlayer < 400 || distanceToPlayer < this.attackRange)) {
                    this.castSI();
                }

                if (distanceToPlayer > this.attackRange) {
                    if (distanceToPlayer <= 400 && this.daggerCD <= 0 && this.player.collidable) {
                        var direction = new me.Vector2d();
                        direction.x = this.player.pos.x - this.pos.x;
                        direction.y = this.player.pos.y - this.pos.y;
                        this.castDagger(direction);
                    }

                    if (this.chargeCD <= 0 && this.player.collidable) {
                        this.charge();
                    }
                }

                else if (distanceToPlayer <= this.attackRange && this.player.collidable) {
                    if  (this.meleeCD <= 0) {
                        this.hit();
                    }
                    else if (this.chargeLeft <= 0) {
                        this.parent();
                        return true;
                    }
                }

                this.walkDirection.x = this.player.pos.x - this.pos.x;
                this.walkDirection.y = this.player.pos.y - this.pos.y;
                this.walkDirection.normalize();

                this.handleAnimations();

            } else {
                this.updateMovement();
                this.parent();
                return true;
            }
            this.vel.x =  this.walkDirection.x * this.speedMovement;
            this.vel.y = this.walkDirection.y * this.speedMovement;
        }
        this.updateMovement();

        var collision = me.game.collide(this);

        if (collision) {
            if (collision.obj.type == 4) {
                if (this.chargeLeft > 0) {
                    this.chargeLeft = 0;
                    this.actionAnimationPending = false;
                    this.speedMovement = 4;
                    this.player.stunned = 2;
                    this.player.health -= this.damageFactor*500;
                }
            }
        }
        this.parent();

        return true;
    },
    changeAnim: function(animName, onComplete) {
        if (this.currentAnim === animName) {
            return;
        }
        var onCompleteAnim = onComplete || null;
        this.currentAnim = animName;
        this.renderable.setCurrentAnimation(animName, onCompleteAnim);
    },
    draw: function(cxt) {
        this.parent(cxt);
        this.text.draw(cxt, this.health.toFixed(0), this.pos.x, this.pos.y);
    },

    handleAnimations: function() {
        if (this.actionAnimationPending) {
            return;
        }
        if (this.walkDirection.x === 0 && this.walkDirection.y === 0) {
            this.renderable.animationpause = true;
        } else {
            if (this.walkDirection.x > 0 && this.walkDirection.x > Math.abs(this.walkDirection.y)) {
                this.changeAnim("walk-right");
                this.animationDirection = "right";
            } else if (this.walkDirection.x < 0 && -this.walkDirection.x > Math.abs(this.walkDirection.y)) {
                this.changeAnim("walk-left");
                this.animationDirection = "left";
            } else if (this.walkDirection.y > 0) {
                this.changeAnim("walk-down");
                this.animationDirection = "down";
            } else if (this.walkDirection.y < 0) {
                this.changeAnim("walk-up");
                this.animationDirection = "up";
            }
            this.renderable.animationpause = false;
        }
    },
    decrementCharging: function() {
        var previous = this.chargeLeft;
        this.chargeLeft -= this.CHARGING_SPEED;

        if (previous > 0 && this.chargeLeft <= 0) {
            this.actionAnimationPending = false;
            this.speedMovement = 4;
            if (this.pos.distance(this.player.pos) <= this.attackRange) {
                this.player.health -= this.damageFactor * 400;
                this.player.stunned = 2;
            }
        }

    }
});
