/*This file is part of mini-Arkhados.

mini-Arkhados is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

mini-Arkhados is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with mini-Arkhados.  If not, see <http://www.gnu.org/licenses/>.*/

var Spells = Spells || {};

Spells.Fireball = me.ObjectEntity.extend({
    speedMovementScaler: new me.Vector2d(450 * 0.017, 450 * 0.017),
    range: 400,
    init: function(xCoord, yCoord) {
        var settingsObj = {
            name: 'fireball',
            image: 'fireball',
            spritewidth: 16,
            spriteheight: 16
        };
        this.parent(xCoord, yCoord, settingsObj);
        this.traveledDistance = 0;
        this.alwaysUpdate = true;
    },
    update: function() {
        var collisions,
            i;
        this.updateMovement();
        this.traveledDistance += this.speedMovementScaler.x;

        if (this.traveledDistance > this.range) {
            me.game.remove(this);
        }

        collisions = this.collide(true);
        if (collisions) {
            for (i = 0; i < collisions.length; ++i) {
                if (collisions[i].type == me.game.ENEMY_OBJECT) {
                    me.game.remove(this);
                    collisions[i].obj.health -= 160;
                    me.audio.play('explosion3');
                    var explosionGfx = me.entityPool.newInstanceOf('explosion_gfx', this.pos.x - 16, this.pos.y - 16);
                    me.game.add(explosionGfx, 4);
                }
            }
        }

        this.parent();

        return true;
    },
    setDirection: function(direction) {
        this.vel.setV(direction);
        this.vel.normalize();
        this.vel.scale(this.speedMovementScaler);
    }
});

Spells.Firewalk = me.ObjectEntity.extend({
    // NOTE: These are constants or static variables
    range: 300,
    speedMovementScaler: new me.Vector2d(500 * 0.017, 500 * 0.017),
    targetLocation: new me.Vector2d(),

    init: function(x, y) {
        var settingsObj = {
            name: 'firewalk',
            image: 'fireball',  // Temporarily use fireball
            spritewidth: 16,
            spriteheight: 16
        };
        this.parent(x, y, settingsObj);
        this.traveledDistance = 0;
        this.distanceToTravel = 0;
        this.player = me.game.getEntityByName("mainPlayer")[0];
        this.player.collidable = false;
        this.player.visible = false;
        this.alwaysUpdate = true;

        this.damageDealt = false;
    },
    update: function() {
        this.updateMovement();

        this.traveledDistance += this.speedMovementScaler.x;
        if (this.traveledDistance >= this.distanceToTravel) {
            this.end();
        }

        if (!this.damageDealt) {
            var collisions = this.collide(true);

            if (collisions) {
                for (i = 0; i < collisions.length; ++i) {
                    if (collisions[i].type == me.game.ENEMY_OBJECT) {
                        collisions[i].obj.health -= 180;
                        this.damageDealt = true;
                    }
                }
            }
        }

        this.parent();

        this.player.pos.setV(this.pos);
        return true;
    },
    setTargetLocation: function(targetLoc) {
        var displacement = targetLoc.clone().sub(this.pos);

        if (displacement.length() >= this.range) {
            this.distanceToTravel = this.range;
        } else {
            this.distanceToTravel = displacement.length();
        }

        displacement.normalize();
        displacement.scale(this.speedMovementScaler);
        this.vel.setV(displacement);
    },
    end: function() {
        this.player.pos.setV(this.pos);
        this.player.collidable = true;
        this.player.visible = true;
        me.game.remove(this);

    }
});

Spells.MagmaBash = me.ObjectEntity.extend({
    speedMovementScaler: new me.Vector2d(500 * 0.017, 500 * 0.017),
    range: 400,
    init: function(xCoord, yCoord) {
        var settingsObj = {
            name: 'magmabash',
            image: 'magmabash',
            spritewidth: 16,
            spriteheight: 16
        };
        this.parent(xCoord, yCoord, settingsObj);
        this.traveledDistance = 0;
        this.alwaysUpdate = true;
    },
    update: function() {
        this.updateMovement();
        this.traveledDistance += this.speedMovementScaler.x;

        if (this.traveledDistance > this.range) {
            me.game.remove(this);
        }

        var collisions = this.collide(true);

        if (collisions) {
            for (i = 0; i < collisions.length; ++i) {
                if (collisions[i].type == me.game.ENEMY_OBJECT) {
                    me.game.remove(this);
                    collisions[i].obj.health -= 80;
                    collisions[i].obj.stunned = 1;
                    me.audio.play("explosion4");
                }
            }
        }

        this.parent();

        return true;
    },
    setDirection: function(direction) {
        this.vel.setV(direction);
        this.vel.normalize();
        this.vel.scale(this.speedMovementScaler);
    }
});

Spells.EmberCircle = me.ObjectEntity.extend({

    init: function(x, y) {
        var settingsObj = {
            name: 'embercircle',
            image: 'embercircle',
            spritewidth: 256,
            spriteheight: 128
        };

        this.parent(x, y, settingsObj);
        this.collisionBox.adjustSize(32, 192, 16, 96);

        this.renderable.addAnimation("loop", [5, 6, 7, 8]);
        this.renderable.setCurrentAnimation("loop");
        this.duration = 4;
        this.alwaysUpdate = true;
    },

    update: function() {
        var dt = 0.017,
            collision,
            i;
        this.duration -= dt;
        if (this.duration <= 0) {
            me.game.remove(this);
        }

        collisions = this.collide(true);
        if (collisions) {
            for (i = 0; i < collisions.length; ++i) {
                if (collisions[i].type == me.game.ENEMY_OBJECT) {
                    collisions[i].obj.health -= (dt * 100);
                }
            }
        }
        this.parent();
        return true;
    }
});

Spells.Dagger = me.ObjectEntity.extend({
    speedMovementScaler: new me.Vector2d(650 * 0.017, 650 * 0.017),
    range: 400,
    init: function(xCoord, yCoord) {
        var settingsObj = {
            name: 'shortsword',
            image: 'shortsword',
            spritewidth: 32,
            spriteheight: 32
        };
        this.parent(xCoord, yCoord, settingsObj);
        this.traveledDistance = 0;
        this.alwaysUpdate = true;
        me.audio.play("sword_throw", false, null, 0.5);
    },
    update: function() {
        this.updateMovement();
        this.traveledDistance += this.speedMovementScaler.x;

        if (this.traveledDistance > this.range) {
            me.game.remove(this);
        }

        var collisions = this.collide(true);

        if (collisions) {
            for (i = 0; i < collisions.length; ++i) {
                if (collisions[i].type == 4) {
                    var p = me.game.getEntityByName("mainPlayer")[0];
                    var b = me.game.getEntityByName("enemyPlayer")[0];
                    me.game.remove(this);

                    collisions[i].obj.health -= b.damageFactor*100;
                    var currSpeed = 4;
                    p.speedMovement = 0.75*currSpeed;
                    p.slowDuration = 5;
                }
            }
        }

        this.parent();

        return true;
    },
    setDirection: function(direction) {
        this.vel.setV(direction);
        this.vel.normalize();
        this.vel.scale(this.speedMovementScaler);
    }
});
