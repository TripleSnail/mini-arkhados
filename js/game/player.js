/*This file is part of mini-Arkhados.

mini-Arkhados is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

mini-Arkhados is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with mini-Arkhados.  If not, see <http://www.gnu.org/licenses/>.*/

game.PlayerEntity = me.ObjectEntity.extend({
    walkDirection: new me.Vector2d(),
    speedMovement: 4,
    stunned: 0,
    maxHealth: 2000,
    fireballCooldown: 0,
    firewalkCooldown: 0,
    magmabashCooldown: 0,
    embercircleCooldown: 0,
    currentAnim: "",
    slowDuration: 0,
    type: 4,

    init: function(x, y, settings) {
        this.parent(x, y, settings);
        this.health = this.maxHealth;
        this.anchorPoint.x = 0.5;
        this.anchorPoint.y = 1;
        this.pos.x = x;
        this.pos.y = y;
        me.game.viewport.follow(this.pos, me.game.viewport.AXIS_BOTH);
        this.text = new me.Font("Verdana", 14, "green");
        this.alwaysUpdate = true;
        this.collidable = true;
        this.type = 4;
        this.renderable.addAnimation("walk-up", [104,105,106,107,108,109,110,111,112]);
        this.renderable.addAnimation("walk-left", [117,118,119,120,121,122,123,124,125]);
        this.renderable.addAnimation("walk-down", [130,131,132,133,134,135,136,137,138]);
        this.renderable.addAnimation("walk-right",[143,144,145,146,147,148,149,150,151]);

        this.renderable.addAnimation("death", [260,261,262,263,264,265]);

        this.ownAlive = true;
    },
    draw: function(cxt) {
        this.parent(cxt);
        this.text.draw(cxt, this.health.toFixed(0), this.pos.x, this.pos.y);
    },
    update: function() {
        if (!this.ownAlive) {
            this.parent();
            return true;
        }
        var clickLoc,
            walkDirection,
            targetPosition,
            dt = 0.017;
        if (this.health <= 0) {
            var self = this;
            this.ownAlive = false;
            this.renderable.animationpause = false;
            this.changeAnim("death", function() {
                game.endMatch();
                me.game.remove(self);
            });
            return true;
        }

        if (this.slowDuration <= 0) {
            this.speedMovement = 4;
        }
        this.stunned -= dt;
        walkDirection = InputManager.determineWalkDirection();
        if (this.stunned <= 0) {
            this.renderable.animationpause = false;
            this.vel.x = walkDirection.x * this.speedMovement;
            this.vel.y = walkDirection.y * this.speedMovement;
        } else {
            this.vel.x = 0;
            this.vel.y = 0;
        }

        if (walkDirection.x === 0 && walkDirection.y === 0) {
            this.renderable.animationpause = true;
        } else {
            if (walkDirection.x > 0 && walkDirection.y == 0) {
                this.changeAnim("walk-right");
            } else if (walkDirection.x < 0 && walkDirection.y === 0) {
                this.changeAnim("walk-left");
            } else if (walkDirection.y > 0) {
                this.changeAnim("walk-down");
            } else if (walkDirection.y < 0) {
                this.changeAnim("walk-up");
            }

            this.renderable.animationpause = false;

        }

        if (this.stunned <= 0 && this.collidable) {
            if (me.input.isKeyPressed('m1') && this.fireballCooldown <= 0) {
                clickLoc = InputManager.getTargetPosition();
                this.castFireball(clickLoc);
            } else if (me.input.isKeyPressed('space') && this.firewalkCooldown <= 0) {
                clickLoc = InputManager.getTargetPosition();
                this.castFirewalk(clickLoc);
            } else if (me.input.isKeyPressed('m2') && this.magmabashCooldown <= 0) {
                clickLoc = InputManager.getTargetPosition();
                this.castMagmaBash(clickLoc);
            } else if (me.input.isKeyPressed('q') && this.embercircleCooldown <= 0) {
                clickLoc = InputManager.getTargetPosition();
                this.castEmberCircle(clickLoc);
            }
        }

        this.firewalkCooldown -= dt;
        this.fireballCooldown -= dt;
        this.magmabashCooldown -= dt;
        this.embercircleCooldown -= dt;
        this.slowDuration -= dt;

        this.updateMovement();
        this.parent();
        return true;
    },

    changeAnim: function(animName, onComplete) {
        if (this.currentAnim === animName) {
            return;
        }
        var onCompleteAnim = onComplete || null;
        this.currentAnim = animName;
        this.renderable.setCurrentAnimation(animName, onCompleteAnim);
    },

    castFireball: function(clickLoc) {
        var startPos = this.pos.clone(),
            fireball;

        startPos.x += 24;
        startPos.y += 32;

        fireball = me.entityPool.newInstanceOf('fireball', startPos.x , startPos.y);

        me.game.add(fireball, 4);
        me.audio.play("fireball_cast", false, null, 0.3);
        var direction = clickLoc.clone().sub(startPos);
        fireball.setDirection(direction);

        this.fireballCooldown = 1;
    },
    castFirewalk: function(clickLoc) {
        var startPos = this.pos.clone(),
        firewalk;

        startPos.x += 24;
        startPos.y += 32;

        firewalk = me.entityPool.newInstanceOf('firewalk', startPos.x, startPos.y);

        me.game.add(firewalk, 4);

        firewalk.setTargetLocation(clickLoc);
        this.firewalkCooldown = 8;
    },
    castMagmaBash: function(clickLoc) {
        var startPos = this.pos.clone(),
            magmabash;

        startPos.x += 24;
        startPos.y += 32;

        magmabash = me.entityPool.newInstanceOf('magmabash', startPos.x, startPos.y);
        me.game.add(magmabash, 4);

        var direction = clickLoc.clone().sub(startPos);
        magmabash.setDirection(direction);

        this.magmabashCooldown = 5;
    },
    castEmberCircle: function(clickLoc) {
        var embercircle = me.entityPool.newInstanceOf('embercircle', clickLoc.x - 128, clickLoc.y - 64);
        me.game.add(embercircle, 4);
        this.embercircleCooldown = 6;
    }

});
