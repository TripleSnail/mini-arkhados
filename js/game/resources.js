/*This file is part of mini-Arkhados.

mini-Arkhados is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

mini-Arkhados is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with mini-Arkhados.  If not, see <http://www.gnu.org/licenses/>.*/
var game = game || {};

game.resources = [
    //other
    {name: "title_screen", type:"image", src: "img/title_screen.png"},
    {name: "gplv3", type:"image", src: "img/gplv3.png"},
    //sprites
	{name: "player", type:"image", src: "img/mage.png"},
    {name: "enemy", type:"image", src:"img/barbarian.png"},
    {name: "fireball", type: "image", src: "img/fireball.png"},
    {name: "terrain",  type:"image", src: "img/terrain.png"},
    {name: "shortsword",  type:"image", src: "img/shortsword.png"},
    {name: "embercircle", type:"image", src: "img/embercircle.png"},
    {name: "magmabash", type: "image", src: "img/magmabash.png"},
    {name: "fireball_explosion", type: "image", src: "img/explosion.png"},
    //map
    {name: "area01", type: "tmx", src: "img/area0.tmx"},
    //sounds
    {name: "fireball_cast", type: "audio", src: "audio/", channel : 2},
    {name: "explosion3", type: "audio", src: "audio/", channel: 2}, // Might be removed
    {name: "explosion4", type: "audio", src: "audio/", channel: 2},
    {name: "sword_sound", type: "audio", src: "audio/", channel : 2},
    {name: "sword_throw", type: "audio", src: "audio/", channel : 2},
    {name: "fight_growl", type: "audio", src: "audio/", channel : 2},
    {name: "main_menu", type: "audio", src: "audio/", channel : 1},
    {name: "heroism", type: "audio", src: "audio/", channel : 1}
];
