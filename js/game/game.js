/*This file is part of mini-Arkhados.

mini-Arkhados is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

mini-Arkhados is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with mini-Arkhados.  If not, see <http://www.gnu.org/licenses/>.*/
var game = game || {};

game.matchData = {};


game.onload = function() {
    if (!me.video.init("game_screen", 640, 480)) {
        alert("Your browser doesn't support HTML5 canvas");
        return;
    }
    document.getElementById("game_screen");
    document.addEventListener("contextmenu", function(e) {
        if (e.button == 2) {
            e.preventDefault();
            return false;
        }
        return true;
    }, false);

    me.sys.gravity = 0;

    me.audio.init('wav,ogg');
    this.player_won = false;
    me.loader.onload = game.load.bind(game);
    me.loader.preload(game.resources);
    me.state.change(me.state.LOADING);
};

game.load = function() {
    me.state.set(me.state.MENU, new Screens.TitleScreen());
    me.state.set(me.state.PLAY, new Screens.PlayScreen());
    me.state.set(me.state.GAMEOVER, new Screens.GameOverScreen());
    me.entityPool.add("mainPlayer", game.PlayerEntity);
    me.entityPool.add("enemyPlayer", game.EnemyEntity);

    me.entityPool.add("fireball", Spells.Fireball, true);
    me.entityPool.add("firewalk", Spells.Firewalk, true);
    me.entityPool.add("magmabash", Spells.MagmaBash, true);
    me.entityPool.add("embercircle", Spells.EmberCircle, true);
    me.entityPool.add("dagger", Spells.Dagger, true);
    me.entityPool.add("explosion_gfx", Effects.Explosion, true);

    InputManager.setKeybindings();
    me.state.change(me.state.MENU);
};

game.endMatch = function() {
    var player = me.game.getEntityByName("mainPlayer")[0],
        enemy = me.game.getEntityByName("enemyPlayer")[0],
        damageDealt = enemy.maxHealth - enemy.health,
        damageTaken = player.maxHealth - player.health;

    game.matchData['player_health'] = player.health < 0 ? 0 : player.health;
    game.matchData['enemy_health'] = enemy.health < 0 ? 0 : enemy.health;

    game.matchData['damage_dealt'] = ~~damageDealt;
    game.matchData['damage_taken'] = ~~damageTaken;

    me.state.change(me.state.GAMEOVER);

};

document.addEventListener("DOMContentLoaded", game.onload, false);
