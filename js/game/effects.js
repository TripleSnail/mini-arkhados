/*This file is part of mini-Arkhados.

mini-Arkhados is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

mini-Arkhados is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with mini-Arkhados.  If not, see <http://www.gnu.org/licenses/>.*/

var Effects = Effects || {};

Effects.Explosion = me.ObjectEntity.extend({

    init: function(x, y) {
        var settingsObj = {
            name: 'explosion_gfx',
            image: 'fireball_explosion',
            spritewidth: 64,
            spriteheight: 64
        };
        this.parent(x, y, settingsObj);
        this.collidable = false;
        this.renderable.animationpause = true;
    },

    update: function() {
        if (this.renderable.animationpause) {
            this.renderable.animationpause = false;
            var self = this;
            this.renderable.setCurrentAnimation("default", function() {
                me.game.remove(self);
            });
        }
        this.parent();
        return true;
    }
});
