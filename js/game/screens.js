/*This file is part of mini-Arkhados.

mini-Arkhados is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

mini-Arkhados is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with mini-Arkhados.  If not, see <http://www.gnu.org/licenses/>.*/

var Screens = Screens || {};

Screens.TitleScreen = me.ScreenObject.extend({
    init: function() {
        this.parent(true);
        this.title = null;
        this.font = null;
        this.font2 = null;
        this.gpl = null;
        me.audio.play("main_menu");
    },

    onResetEvent: function() {
        if (this.title == null) {
            this.title = me.loader.getImage("title_screen");
            this.gpl = me.loader.getImage("gplv3");
            this.font = new me.Font("Arial", 32, "white");
            this.font2 = new me.Font("Arial", 16, "white");
        }

        me.input.bindKey(me.input.KEY.ENTER, "enter", true);
    },

    update: function() {
        if (me.input.isKeyPressed('enter')) {
            me.state.change(me.state.PLAY);
            me.audio.stop("main_menu");
            me.audio.play("heroism", true);
        }
        return true;
    },

    draw: function(ctx) {
        ctx.drawImage(this.title, 0, 0);
        ctx.drawImage(this.gpl, 100, 100);
        this.font.draw(ctx, "Press ENTER to play", 145, 203);
        this.font2.draw(ctx, "Keys: ", 145, 280);
        this.font2.draw(ctx, "Fireball: M1", 145, 300);
        this.font2.draw(ctx, "Magma bash: M2", 145, 320);
        this.font2.draw(ctx, "Firewalk: Space", 145, 340);
        this.font2.draw(ctx, "Ember circle: Q", 145, 360);
    },

    onDestroyEvent: function() {
        me.input.unbindKey(me.input.KEY.ENTER);
   }
});

Screens.PlayScreen = me.ScreenObject.extend({
    onResetEvent: function() {
        me.levelDirector.loadLevel("area01");
    },
    onDestroyEvent: function() {
    }
});

Screens.GameOverScreen = me.ScreenObject.extend({
    init: function() {
        this.parent(true);
        this.title = null;
        this.font = null;
        this.font2 = null;

    },

    onResetEvent: function() {
        if (this.title == null) {
            this.title = me.loader.getImage("title_screen");
            this.font = new me.Font("Arial", 32, "white");
            this.font2 = new me.Font("Arial", 24, "red");
            this.font3 = new me.Font("Arial", 24, "green");
            this.font4 = new me.Font("Arial", 16, "white");
        }
        //me.audio.stop("heroism");
        me.input.bindKey(me.input.KEY.ENTER, "enter", true);
    },

    update: function() {
        if (me.input.isKeyPressed('enter')) {
            me.audio.stop("heroism");
            me.audio.play("main_menu");
            me.state.change(me.state.MENU);
        }
        return true;
    },

    draw: function(ctx) {
        ctx.drawImage(this.title, 0, 0);
        this.font.draw(ctx, "GAME OVER", 210, 103);
        if (!me.player_won) {
            this.font2.draw(ctx, "You lost!", 275, 140);
        } else {
            this.font3.draw(ctx, "You won", 280, 140);
        }

        this.font4.draw(ctx, "Damage done: " + game.matchData.damage_dealt, 245, 180);
        this.font4.draw(ctx, "Damage taken: " + game.matchData.damage_taken, 245, 200);

        this.font4.draw(ctx, "Press enter to return to the main menu", 165, 240);

        this.font4.draw(ctx, "Ps: checkout https://github.com/dnyarri/Arkhados :)", 130, 280);
    },

    onDestroyEvent: function() {
        me.input.unbindKey(me.input.KEY.ENTER);
   }
});
