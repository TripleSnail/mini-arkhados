/*This file is part of mini-Arkhados.

mini-Arkhados is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

mini-Arkhados is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with mini-Arkhados.  If not, see <http://www.gnu.org/licenses/>.*/

var InputManager =  InputManager || {};

InputManager.targetPosition = new me.Vector2d();

InputManager.setKeybindings = function() {
    me.input.bindKey(me.input.KEY.A, "left");
    me.input.bindKey(me.input.KEY.D, "right");
    me.input.bindKey(me.input.KEY.S, "down");
    me.input.bindKey(me.input.KEY.W, "up");

    // Lock is set to true temporarily
    me.input.bindKey(me.input.KEY.NUM0, "m1");
    me.input.bindKey(me.input.KEY.NUM1, "m2");

    me.input.bindMouse(me.input.mouse.LEFT, me.input.KEY.NUM0);
    me.input.bindMouse(me.input.mouse.RIGHT, me.input.KEY.NUM1);

    me.input.bindKey(me.input.KEY.SPACE, "space");
    me.input.bindKey(me.input.KEY.Q, "q");
};

InputManager.walkDirection = new me.Vector2d();

// Irrelevant for now
InputManager.keyUpFlags = {"left": false,
                           "right": false,
                           "down": false,
                           "up": false};

InputManager.determineWalkDirection = function() {
    this.walkDirection.x = 0;
    this.walkDirection.y = 0;

    if (me.input.isKeyPressed('left')) {
        this.walkDirection.x -= 1;
    }
    if (me.input.isKeyPressed('right')) {
        this.walkDirection.x += 1;
    }

    if (me.input.isKeyPressed('up')) {
        this.walkDirection.y -= 1;
    }

    if (me.input.isKeyPressed('down')) {
        this.walkDirection.y += 1;
    }

    this.walkDirection.normalize();
    return this.walkDirection;

};

InputManager.getTargetPosition = function() {
    var mousePos = me.input.mouse.pos,
        x = mousePos.x,
        y = mousePos.y;

    return me.game.viewport.localToWorld(x, y);
};
